export default (
  state = {
    item: {},
    favItems:[],
    isShowDialog: false,
    shoppingCart:[],
    isSignIn:false
  },
  action
) => {
  switch (action.type) {
    case 'click_item':
      return {
        ...state,
        isShowDialog: true,
        item: action.payload
      };
    case 'dismiss_dialog':
      return {
        ...state,
        isShowDialog: false,
        item: {}
      };
      case 'set_favorite':
     
      return {
        ...state,
        favItems: action.payload
      };
      case 'set_cart':
      return {
        ...state,
        shoppingCart: action.payload
      };
      case 'set_SignIn':
      return {
        ...state,
        isSignIn: action.payload,
      };
    default:
      return state;
  }
};
