import React, { Component } from 'react';
import ListFavorite from '../Components/favorite/list';
import { connect } from 'react-redux';
const KEY_USER_DATA = "user_data";
const mapStateToProps = state => {
  return {
    isShowDialog: state.isShowDialog
  };
};

class FavoritePage extends Component {
  getItems = () => {
    const jsonStr = localStorage.getItem(KEY_USER_DATA);
    const email = jsonStr && JSON.parse(jsonStr).email;
    const jsonFavStr = localStorage.getItem(`beer-ja-list-fav-${email}`);
    if (jsonFavStr) {
      const items = jsonFavStr && JSON.parse(jsonFavStr);
      return items;
    }
  };

  render() {
    return (
      <div
        style={{
          padding: '16px',
          marginTop: 64,
          minHeight: '600px'
        }}
      >
        <ListFavorite items={this.getItems()} />
      </div>
    );
  }
}

export default connect(mapStateToProps)(FavoritePage);
