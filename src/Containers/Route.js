import React from 'react';
import { Route, Switch } from 'react-router-dom';
import LoginPage from './Login';
import Main from '../Containers/Main';
import RegisterPage from '../Containers/RegisterPage';
import CartPage from '../Containers/CartPage';
function Routes() {
  return (
    <div style={{ width: '100%' }}>
      <Switch>
        <Route exact path="/" component={LoginPage} />
        <Route exact path="/register" component={RegisterPage} />
        <Route component={Main} />
      </Switch>
    </div>
  );
}
export default Routes;
