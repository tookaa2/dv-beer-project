import React, { Component } from 'react';
import ListCart from '../Components/cart/list';
import { Button, message } from 'antd';
import item from '../Components/card/item';
import _ from 'lodash';
import { connect } from 'react-redux';

const mapStateToProps = state => {
  return {
    isShowDialog: state.isShowDialog,
    item: state.item,
    favItems: state.favItems,
    cartItems: state.shoppingCart
  };
};
class CartPage extends Component {
  state = {
    items: [],
    email: '',
    isDisable: true,
    totalPrice: 0
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem('user_data');
    const email = jsonStr && JSON.parse(jsonStr).email;
    this.setState({ email });
    const jsonCartStr = localStorage.getItem(`beer-ja-list-cart-${email}`);
    if (jsonCartStr) {
      const items = jsonCartStr && JSON.parse(jsonCartStr);
      if (items.length > 0) {
        const totalPrice = this.getTotal(this.props.cartItems);

        this.setState({ items: items, isDisable: false, totalPrice: totalPrice });
      }
    }

  }

  getTotal = (items) => {

    let price = 0;
    _.forEach(items, (value, key) => {
      price += value.price;
    })
    return price;
  }

  onClickCheckout = () => {
    localStorage.setItem(
      `beer-ja-list-cart-${this.state.email}`,
      JSON.stringify([])
    );
    this.setState({ items: [], isDisable: true });
    message.success('You checkout successfully', 1, () => {
      this.props.history.replace('/');
    });
  };

  render() {
    return (
      <div
        style={{
          width: '100%',
          height: '100%',
          minHeight: '600px'
        }}
      >
        <div
          style={{ width: '100%', background: 'rgb(0,21,41)', position: 'relative' }}
        >
          <h1 style={{ color: 'white', padding: '10px' }}>Your Cart</h1>

        </div>
        <ListCart items={this.props.cartItems} />
        <div style={{ display:'flex',width: '100%', background: 'rgb(0,21,41)', height: '80px',   alignItems: 'center', justifyContent: 'center' }}>

          <p style={{fontWeight:'bold',fontSize:'2em',color:'white'}}>Total Price: ฿{this.state.totalPrice}</p>
          <Button
            disabled={this.state.isDisable}
            type="primary"
            style={{
              position: 'absolute',
              right: '80px',
            
              transform: 'translateY(-50%)'
            }}
            onClick={this.onClickCheckout}
          >
            Checkout
          </Button>
        </div>
      </div>
    );
  }
}

// export default CartPage;
export default connect(
  mapStateToProps,
  null
)(CartPage);
