import React, { Component } from "react";
import { Form, Button, Input, Icon, message } from "antd";
import { withRouter } from "react-router-dom";

import { auth, provider }  from '../utils/firebaseHelper.js';
import {KEY_USER_DATA} from '../Constants'



class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { user: null };
  }
  state = {
    email: "",
    password: "",
    errorVisible: false,
    iconLoading:false
  };
  login = () => {
    this.setState({iconLoading:true})
    auth.signInWithPopup(provider)
      .then(function(result) {
        var token = result.credential.accessToken;
        return  result.user;
      }).then(user=>{
        console.log(user)
        localStorage.setItem(
          KEY_USER_DATA,
          JSON.stringify({
            email:user.email,
            imageUrl: `${user.photoURL}?height=500`,
            isLoggedIn: true,
            name:user.displayName
          })
        );
        if (user) {
          this.navigateToMainPage();
        }
      })
      .catch(function(error) {
        this.setState({iconLoading:false})
        var errorCode = error.code;
        var errorMessage = error.message;
        var email = error.email;
        var credential = error.credential;
      });
  };
  componentDidMount() {
    const jsonStr = localStorage.getItem(KEY_USER_DATA);
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
    if (isLoggedIn) {
      this.navigateToMainPage();
    }
  }
  onPressRegister = () => {
    console.log("REDIRECT Register");
    const { history } = this.props;
    history.push("/register");
  };
  navigateToMainPage = () => {
    console.log("REDIRECT");
    const { history } = this.props;
    history.push("/home");
  };
  onEmailChange = event => {
    this.setState({ email: event.target.value });
  };
  onPasswordChange = event => {
    this.setState({ password: event.target.value });
  };
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  validatePassword(password) {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    return re.test(String(password));
  }
  onSubmitForm = event => {
    event.preventDefault();
    const isEmailValid = this.validateEmail(this.state.email);
    if (isEmailValid) {
      if (this.validatePassword(this.state.password)) {

        auth
        .signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(({ user }) => {
         
          localStorage.setItem(
            KEY_USER_DATA,
            JSON.stringify({
              email:this.state.email,
              isLoggedIn: true
            })
          );

         
  
          this.navigateToMainPage();
        })
        .catch(err=> {
          message.error('Wrong username or Password!');
        });



        
      } else {
        console.log("pass ผิด");
      }
    } else {
      this.setState({ errorVisible: true });
      message.error("Email or Password Invalid");
    }
  };
  render() {
    return (
      <div
        style={{
          display: "flex",
          alignContent: "center",
          justifyContent: "center"
        }}
      >
        <div style={{ width: "30%", marginTop: "20%" }}>
          <Form onSubmit={this.onSubmitForm}>
            <Form.Item>
              <Input
                prefix={<Icon type="user" />}
                type="user"
                placeholder="Email"
                onChange={this.onEmailChange}
              />
            </Form.Item>

            <Form.Item>
              <Input
                prefix={<Icon type="lock" />}
                type="password"
                placeholder="Password"
                onChange={this.onPasswordChange}
              />
            </Form.Item>

            <Form.Item>
              <Button htmlType="submit">Submit</Button><Button style={{marginLeft:20}}type="primary" onClick={this.onPressRegister}  >Sign up</Button>
            </Form.Item>
          </Form>
          
          <Button type="primary" icon="facebook" onClick={this.login} loading={this.state.iconLoading} >Login with Facebook</Button>
          <img
            style={{ display: this.state.errorVisible ? "flex" : "none" }}
            src="https://media1.giphy.com/media/qmfpjpAT2fJRK/giphy.gif?cid=3640f6095c4e89f4716e46386f32c2b5"
          />
          <h1 style={{ display: this.state.errorVisible ? "flex" : "none" }}>
            Wrong format :( !!!!!!!!
          </h1>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
