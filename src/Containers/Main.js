import React, { Component } from "react";
import { Spin, Layout, Button, Menu, Modal, message, Badge, Icon } from "antd";
import RouteMenu from './RouteMenu';
import { connect } from 'react-redux';
import { checkIfItemFavorited, FavoriteItem } from '../utils/dataHelper'
import { stat } from "fs";
import { homedir } from "os";
const KEY_USER_DATA = "user_data";
const { SubMenu } = Menu;
const menus = ['home', 'favorite', 'aboutUs'];
const { Header, Content, Footer, Sider } = Layout;


const mapStateToProps = state => {
  return {
    isShowDialog: state.isShowDialog,
    item: state.item,
    favItems: state.favItems,
    cartItems: state.shoppingCart,
    isSignIn: state.isSignIn
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDismissDialog: () =>
      dispatch({
        type: 'dismiss_dialog'
      }),
    setFavorite: (input) =>
      dispatch({
        type: 'set_favorite',
        payload: input
      }),
    setCart: (input) =>
      dispatch({
        type: 'set_cart',
        payload: input
      }),
    setSignIn: (input) =>
      dispatch({
        type: 'set_SignIn',
        payload: input
      })




  };
};

class Main extends Component {
  constructor(props) {
    super(props);
    // const jsonStr = localStorage.getItem(KEY_USER_DATA);
    // const email = jsonStr && JSON.parse(jsonStr).email;
    // const jsonFavStr = localStorage.getItem(`beer-ja-list-fav-${email}`);

    // if (jsonFavStr) {
    //   const items = jsonFavStr && JSON.parse(jsonFavStr);
    //   this.props.setFavorite(items);
    //   console.log(this.props)
    // }

    this.state = {
      favItems: [],
      cartItems: [],
      email: '',
      showSide: true,
      collapsed: false,
      profilePic: null,
      name: null,
      isLoggedIn: false,
    };
  }


  componentWillMount() {
    const jsonStr = localStorage.getItem(KEY_USER_DATA);
    const loggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
    console.log('are they logged?', loggedIn);
    if (loggedIn) {
      this.setState({ isLoggedIn: loggedIn });
      this.props.setSignIn(loggedIn);
    }
    const email = jsonStr && JSON.parse(jsonStr).email;
    console.log(jsonStr);
    const profilePic = jsonStr && JSON.parse(jsonStr).imageUrl
    const name = jsonStr && JSON.parse(jsonStr).name
    if (profilePic) {
      this.setState({ profilePic });
    } else {
      this.setState({ profilePic: 'https://www.flynz.co.nz/wp-content/uploads/profile-placeholder.png' })
    }
    if (name) {
      this.setState({ name });
    } else {
      this.setState({ name: email })
    }
    const { pathname } = this.props.location;
    var currentMenu = menus[0];
    if (pathname != '/') {
      currentMenu = pathname.replace('/', '');
      if (!menus.includes(currentMenu)) currentMenu = menus[0];
    }
    const jsonFavStr = localStorage.getItem(`beer-ja-list-fav-${email}`);
    if (jsonFavStr) {
      const items = jsonFavStr && JSON.parse(jsonFavStr);
      this.props.setFavorite(items);
      console.log(this.props)
    }
    const jsonCartStr = localStorage.getItem(`beer-ja-list-cart-${email}`);
    if (jsonCartStr) {
      const cartItems = jsonCartStr && JSON.parse(jsonCartStr);
      this.setState({ cartItems: cartItems });
      this.props.setCart(cartItems);
    }
    this.setState({ currentMenu, email });
    console.log('current state', this.state)
  }
  navigateToLoginPage = () => {
    const { history } = this.props;
    history.push("/");
  };
  logout = () => {
    localStorage.setItem(
      KEY_USER_DATA,
      JSON.stringify({
        isLoggedIn: false
      })
    );
    this.props.setSignIn(false);
    this.navigateToLoginPage();
  };
  onMenuClick = e => {
    console.log('pressed', e)
    if (e.key) {
      var path = '/';
      path = `/${e.key}`;
      this.props.history.replace(path);
    }
    if (e.key !== 'home') {
      this.onCollapse(true);
    }
  };
  checkItemFavorited = () => {
    if (checkIfItemFavorited(this.props.favItems, this.props.item)) {
      return 'primary';
    } else {
      return '';
    }
  };
  settingState = (inputObject) => {
    this.setState(inputObject)
    this.props.setFavorite(inputObject)
  }
  onClickFavoriteItem = () => {
    if(this.props.isSignIn){
      const favoritedList = this.props.favItems
      FavoriteItem(favoritedList, this.props.item, this.settingState, this.props.onDismissDialog)
    }else{
      message.error('please log in to add favorite')
    }
    
  };
  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  }

  onClickAddToCart = () => {
    if(this.props.isSignIn){
    const items = this.props.cartItems;
    const itemCart = this.props.item;
    items.push(itemCart);
    localStorage.setItem(
      `beer-ja-list-cart-${this.state.email}`,
      JSON.stringify(items)
    );
    message.success('Added to your cart', 1, () => {
      this.setState({ cartItems: items });
      this.props.setCart(items);
      this.props.onDismissDialog()
    });}
    else{
      message.error('Please sign in to shop')
    }
  };

  render() {
    const item = this.props.item;
    return (
      <div>
        <Layout className="layout" style={{ background: 'white' }}>
          <Header
            style={{
              padding: '0px',
              position: 'fixed',
              zIndex: 1,
              width: '100%',

            }}
          >
            <Menu
              theme="light"
              mode="horizontal"
              defaultSelectedKeys={[this.state.currentMenu]}
              style={{ lineHeight: '64px' }}
              onClick={e => {
                this.onMenuClick(e);
              }}
            >
              <Menu.Item key={menus[0]}>Home</Menu.Item>

              <Menu.Item key={menus[2]}>About Us</Menu.Item>


              <div
                style={{
                  position: 'absolute',
                  top: '50%',
                  right: '46px',
                  transform: 'translateY(-40%)'
                }}
              >
                <Badge
                  count={this.props.cartItems.length}
                  overflowCount={10}
                  key={'/cart'}
                  onClick={() => {
                    this.props.history.replace('/cart');

                  }}
                >
                  <Icon type="shopping-cart" style={{ fontSize: '30px' }} />
                </Badge>
              </div>
            </Menu>

          </Header>
          <Layout>
            <Sider style={{ paddingTop: '70px', display: this.state.showSide ? 'inline' : 'none' }} collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
              <div style={{ position: 'sticky', top: 70 }}>
                {this.props.isSignIn ?
                  <div>
                    <p style={{ color: 'white' }}>{this.state.collapsed ? '' : 'Welcome!'}</p>
                    <img src={this.state.profilePic} style={{ width: '80%', borderRadius: '50%' }} />
                    <p style={{ color: 'white', marginTop: '8px' }}>{this.state.collapsed ? '' : this.state.name}</p>
                    <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" inlineCollapsed={this.state.collapsed}  >
                      <Menu.Item key="1" onClick={() => this.onMenuClick({ key: 'favorite' })}>
                        <Icon type="heart" />
                        <span >Favorite List</span>
                      </Menu.Item>
                      <SubMenu title={<span><Icon type="setting" /><span>Settings</span></span>} key="sub1">
                        <Menu.Item key="5" onClick={this.logout}>Sign Out</Menu.Item>

                      </SubMenu>
                    </Menu>
                  </div> :<div>
                  <p style={{ color: 'white' }}>
                    Sign In to enjoy shopping
                </p>
                <Button
                  key="signin"
                  type="primary"
                  size="large"
                
                  onClick={this.navigateToLoginPage}
                >Sign In</Button>
                </div>
                }
              </div>
            </Sider>
            <Content
              style={{
                padding: '16px',
                marginTop: 64,
                minHeight: '600px',
                justifyContent: 'center',
                alignItems: 'center',
                display: 'flex'
              }}
            >
              <RouteMenu items={this.state.items} />
            </Content>

          </Layout>

        </Layout>

        {item != null ? (
          <div>
            <Modal
              width="40%"
              style={{ maxHeight: '70%' }}
              title={item.name}
              visible={this.props.isShowDialog}
              onCancel={this.props.onDismissDialog}
              footer={[
                <Button
                  key="submit"
                  type={this.checkItemFavorited()}
                  icon="heart"
                  size="large"
                  shape="circle"
                  onClick={this.onClickFavoriteItem}
                />,
                <Button
                  key="submit"
                  type="primary"
                  icon="shopping-cart"
                  size="large"
                  shape="circle"
                  onClick={this.onClickAddToCart}
                />
              ]}
            >
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: '16px'
                }}
              >
                <img
                  src={item.image_url}
                  style={{ height: '200px', width: 'auto' }}
                />
              </div>
              <p>
                <b>Tagline:</b> {item.tagline}
              </p>
              <p>
                <b>First Brewed:</b> {item.first_brewed}
              </p>
              <p>
                <b>Description:</b> {item.description}
              </p>
              <p>
                <b>Brewers Tips:</b> {item.brewers_tips}
              </p>
              <p>
                <b>Contributed by:</b> {item.contributed_by}
              </p>
            </Modal>
          </div>
        ) : (
            <div />
          )}
      </div>
    );
  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);

