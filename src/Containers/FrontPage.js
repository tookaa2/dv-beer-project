import React, { Component } from "react";
import { Spin, Pagination } from "antd";
import CardList from "../Components/card/list";
import BannerAnim from "rc-banner-anim";
import TweenOne from "rc-tween-one";
import "./banner.css";
import "rc-banner-anim/assets/index.css";
import _ from 'lodash';
const { Element } = BannerAnim;
const BgElement = Element.BgElement;
class FrontPage extends Component {
  state = {
    items: [],
    page: 1,
    perPage: 40,
    isLoading: true
  };

  addPrice = (items) => {
    const standardPrice = [79, 288, 786, 122, 145, 100, 99, 89, 45, 789, 654, 899, 1985, 123, 584, 125, 456, 958, 289, 329, 690, 451, 288, 900, 1018];
    let output = []
    let indexPrice = 0;
    _.forEach(items, (value, key) => {
      let objectBeer= _.cloneDeep(value);
      if (indexPrice >= standardPrice.length) {
        indexPrice = 0;
      }
      objectBeer.price = standardPrice[indexPrice];
      output.push(objectBeer);
      indexPrice++;
    })
    
    return output;
  }

  componentDidMount() {
    this.loadBeer();
  }
  loadBeer = () => {
    this.setState({ isLoading: true });
    fetch(
      `https://api.punkapi.com/v2/beers?page=${this.state.page}&per_page=${
        this.state.perPage
      }`
    )
      .then(response => response.json())
      .then(items => this.setState({ items: this.addPrice(items), isLoading: false }));
  };
  onPageChange = (page, pageSize) => {
    this.setState({ page, perPage: pageSize }, () => {
      this.loadBeer();
    });
  };
  render() {
    return (
      <div>
        <BannerAnim prefixCls="banner-user" autoPlay autoPlaySpeed={3000} arrow={false}>
          <Element prefixCls="banner-user-elem" key="a">
            <BgElement
              key="bg"
              className="bg"
              style={{
                backgroundImage:
                  "url(https://prods3.imgix.net/images/articles/2016_05/Feature-Mexican-Beer-Tecate-Modelo-Dos-Equis-Vitoria-Ambar.jpg)",
                backgroundSize: "cover",
                backgroundPosition: "center"
              }}
            />

            <TweenOne key="0" style={{ position: "absolute", top: 0 }}>
            <h1>Largest online beer store</h1>
              
            </TweenOne>
          </Element>
          <Element prefixCls="banner-user-elem" key="b">
            <BgElement
              key="bg"
              className="bg"
              style={{
                backgroundImage:
                  "url(https://dc8gwjuur0w0x.cloudfront.net/lists/avatars/000/000/047/original_o-PINT-GLASS-BEER-facebook.jpg?1473216955)",
                backgroundSize: "cover",
                backgroundPosition: "center"
              }}
            />
            <TweenOne key="0"><p style={{fontSize:'50px',position:'relative',top:'150px'}}>Celebrate your weekend!</p></TweenOne>
          </Element>
        </BannerAnim>

        {this.state.items.length > 0 ? (
          <div>
            <CardList items={this.state.items} />
            <Pagination
              total={240}
              pageSize={40}
              defaultCurrent={this.state.page}
              onChange={this.onPageChange}
            />
          </div>
        ) : (
          <Spin size="large" />
        )}
      </div>
    );
  }
}

export default FrontPage;
