import React from 'react';
import { Route, Switch } from 'react-router-dom';
import FrontPage from './FrontPage';
import AboutUs from './AboutUs';
import FavoritePage from './FavoritePage';
import CartPage from './CartPage';
function RouteMenu() {
  return (
    <div style={{ width: '100%' }}>
      <Switch>
        <Route exact path="/home" component={FrontPage} />
        <Route exact path="/favorite" component={FavoritePage} />
        <Route exact path="/aboutUs" component={AboutUs} />
        <Route exact path="/cart" component={CartPage} />
      </Switch>
    </div>
  );
}

export default RouteMenu;