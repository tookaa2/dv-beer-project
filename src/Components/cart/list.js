import React, { Component } from 'react';
import { List, Avatar } from 'antd';

class ListFavorite extends Component {
  render() {
    return (
      <div style={{ minHeight: '300px', textAlign: 'left' }}>
        <List
          style={{
            marginLeft: '200px',
            marginRight: '200px',
            marginTop: '16px'
          }}
          itemLayout="vertical"
          size="large"
          dataSource={this.props.items}
          renderItem={item => (
            <List.Item
              key={item.name}
              style={{ paddingTop: '20px' }}
              extra={
                <div style={{ width: '80px', height: '150px' }}>
                  <img
                    // width="auto"
                    // height="auto"
                    alt="logo"
                    src={item.image_url}
                    style={{
                      maxWidth: '100%',
                      maxHeight: '100%'
                    }}
                    resizeMode="contain"
                  />
                </div>
              }
            >
              <List.Item.Meta
                style={{ display: 'flex' }}
                title={<a href={item.href}>{item.name}</a>}
                description={<div style={{ height: '100%', display: 'flex' }}>
                  <div style={{ flex: 3 }}>
                    {item.description}
                  </div>
                  <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-end' }}>
                    <p style={{ fontWeight: 'bold' }}>฿{item.price}</p>

                  </div>

                </div>}
              />
              {item.content}
            </List.Item>
          )}
        />
      </div>
    );
  }
}

export default ListFavorite;
