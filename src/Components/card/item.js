import React, { Component } from "react";
import { Card, message, Icon } from "antd";
import TextTruncate from "react-text-truncate";
import { connect } from "react-redux";
import {KEY_USER_DATA} from '../../Constants'
import {FavoriteItem,checkIfItemFavorited} from '../../utils/dataHelper';
const { Meta } = Card;

const mapStateToProps = state => {
  return {
    favItems: state.favItems,
    cartItems:state.shoppingCart,
    isSignIn: state.isSignIn
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onItemBeerClick: item =>
      dispatch({
        type: "click_item",
        payload: item
      }),
      setFavorite: (input) =>
      dispatch({
        type: 'set_favorite',
        payload: input
      }),
      setCart: (input) =>
      dispatch({
        type: 'set_cart',
        payload: input
      }),
  };
};


class Item extends Component {
  constructor(props) {
    super(props);
    this.state = { hover: false };
  }
  getItems = () => {
    const jsonStr = localStorage.getItem(KEY_USER_DATA);
    const email = jsonStr && JSON.parse(jsonStr).email;
    const jsonFavStr = localStorage.getItem(`beer-ja-list-fav-${email}`);
    if (jsonFavStr) {
      const items = jsonFavStr && JSON.parse(jsonFavStr);
      return items;
    }
  };
  onClickAddToCart = () => {
    if(this.props.isSignIn){

      const items = this.props.cartItems;
      const itemCart = this.props.item;
      items.push(itemCart);
    
      const jsonStr = localStorage.getItem(KEY_USER_DATA);
      const email = jsonStr && JSON.parse(jsonStr).email;
      localStorage.setItem(
        `beer-ja-list-cart-${email}`,
        JSON.stringify(items)
      );
      this.props.setCart(items);
      message.success('Added to your cart', 1);
    }else{
      message.error('Please sign in to shop')
    }
   
  };
  checkItemFavorited = (givenList) => {
    return checkIfItemFavorited(givenList,this.props.item);
  };
  settingState = (inputObject) => {
    this.props.setFavorite(inputObject)
  }
  onClickFav=()=>{
    if(this.props.isSignIn){
    const items = this.props.favItems;
    const item = this.props.item;
    FavoriteItem(items, item,this.settingState);
    }else{
      message.error('Please sign in o favorite')
    }
  }
  render() {
    const props = this.props;
    return (
      <Card

        hoverable
        cover={
          <div>
            <img
              onClick={() => {
                props.onItemBeerClick(props.item);
              }}
              onMouseEnter={() => {
                this.setState({ hover: true })
              }}
              onMouseLeave={() => {

                this.setState({ hover: false })
              }}
              src={props.item.image_url}
              style={{ height: "200px", top: this.state.hover ? '15px' : '0px', width: "auto", paddingTop: this.state.hover ? "1px" : "16px", position: 'relative' }}
            />
          </div>
        }
        actions={[<div onClick={this.onClickFav}>{
          this.checkItemFavorited(this.props.favItems) ?
            <Icon type="heart"  theme="twoTone" twoToneColor="#eb2f96"/> : <Icon type="heart" />
        }

        </div>, <Icon onClick={this.onClickAddToCart}type="shopping-cart" />]}
      >
        <Meta
          title={props.item.name}
          description={
            <div>
              <p style={{ fontWeight: 'bold', color: 'black' }}>
                {'฿ ' + props.item.price}
              </p>

              <TextTruncate
                line={2}
                truncateText="…"
                text={props.item.description}
                textTruncateChild={<a href="#" onClick={() => {
                  props.onItemBeerClick(props.item);
                }}>Read more</a>}
              />
            </div>
          }
        />
      </Card>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Item);
