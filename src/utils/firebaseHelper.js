import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyABwUBTW3ApDVQbCyYTSaT5ODP1Pi01ODw",
    authDomain: "dv-workshop-61dab.firebaseapp.com",
    databaseURL: "https://dv-workshop-61dab.firebaseio.com",
    projectId: "dv-workshop-61dab",
    storageBucket: "dv-workshop-61dab.appspot.com",
    messagingSenderId: "842319182623"
};

firebase.initializeApp(config);

const database = firebase.database();
const auth = firebase.auth();
const provider = new firebase.auth.FacebookAuthProvider();

export { database, auth, provider };
