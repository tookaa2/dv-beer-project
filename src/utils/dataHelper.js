import { Spin, Layout, Button, Menu, Modal, message,Badge,Icon } from "antd";
import {KEY_USER_DATA} from '../Constants'
export const checkIfItemFavorited = (listOfFavorited,itemToCheck) => {
    const result = listOfFavorited.find(itemInput => {
      return itemInput.name === itemToCheck.name;
    });
    if (result) {
      return true;
    } else {
      return false;
    }
  };


 export const FavoriteItem = (favoritedItems,itemToFav,setState,callback) => {
    
    const items = favoritedItems;
    const itemFav = itemToFav;
    const index = items.findIndex(item => {
      return item.name === itemFav.name;
    });
    if (index != -1) {
      items.splice(index, 1);
      localStorage.setItem(
        `beer-ja-list-fav-${getEmail()}`,
        JSON.stringify(items)
      );
      message.success('unfavorite this item successfully', 1, () => {
          if(setState){
            setState(items);
          }
        
        if(callback){
            callback()
        }
     
      });
    } else {
      items.push(itemFav);
      localStorage.setItem(
        `beer-ja-list-fav-${getEmail()}`,
        JSON.stringify(items)
      );
      message.success('Saved your favorite this item', 1, () => {
          if(setState){
            setState(items);
          }
        
        if(callback){
            callback()
        }
      });
    }
  };

  export const getEmail=()=>{
    const jsonStr = localStorage.getItem(KEY_USER_DATA);
    return jsonStr && JSON.parse(jsonStr).email;
  }